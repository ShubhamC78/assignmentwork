import React, { Component } from 'react'
import { Button, Text, TouchableOpacity, View, BackHandler, StyleSheet, StatusBar, Alert } from 'react-native'

export default class Login extends Component {

    onPressHome() {
        this.props.navigation.push('Home');
    }

    onPressExit() {
        Alert.alert(
            "Memory game",
            "Do you want to exit application.",
            [
                {
                    text: "Cancel",
                    onPress: () => console.log("Cancel Pressed"),
                    style: "cancel"
                },
                { text: "OK", onPress: () => BackHandler.exitApp() }
            ]
        );
    }


    render() {
        return (
            <View style={styles.containerStyle}>
                <StatusBar backgroundColor="#f1d4ff" barStyle='dark-content' />
                <Text style={{ textAlign: 'center', fontSize: 30, fontWeight: 'bold' }}>
                    Pet {'\n'}
                    Application
                </Text>
                <View >
                    <TouchableOpacity style={styles.buttonViewStyle} onPress={() => { this.onPressHome() }}>
                        <Text style={styles.buttonTextStyle}>
                            Home
                        </Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    containerStyle: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "#f1d4ff",
        flex: 1
    },
    buttonViewStyle: {
        backgroundColor: '#b222f5',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 5,
        marginTop: 20
    },
    buttonTextStyle: {
        marginHorizontal: 30,
        marginVertical: 5,
        fontSize: 20,
        fontWeight: '700',
        color: '#ffffff'
    }
});