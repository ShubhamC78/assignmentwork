import {combineReducers} from 'redux';
import petData from './pet.reducer';

export default combineReducers({
    petState: petData
});