import { ADD_PET, EDIT_PET, DELETE_PET } from '../actions/index.js';

const initialState = {
    petList:
        [
            {
                "Id": 1,
                "Name": "Golden Retriever",
                "Age": "4 Years",
                "Description": "Golden Retrievers are a very popular kind of dog. When the breed was first created, they were used for hunting."
            },
            {
                "Id": 2,
                "Name": "Briard",
                "Age": "5 Years",
                "Description": "The Briard (or Berger de Brie, Shepherd (dog) from Brie) is a French kind of dog. Originally, the Briard was used for herding livestock. "
            },
            {
                "Id": 3,
                "Name": "Dalmatian",
                "Age": "2 Years",
                "Description": "The Dalmatian is a medium , strong dog. Its skull is flat on the top. "
            },

        ]

}


const petData = (state = initialState, action) => {
    switch (action.type) {
        case ADD_PET: {
            let tempList = state.petList;
            console.log(tempList)
            tempList.push({
                "Id": tempList.length + 1,
                "Name": action.payload.Name,
                "Age": action.payload.Age,
                "Description": action.payload.Description,
            })
            return {
                ...state, petList: tempList
            }
        }
        case EDIT_PET: {
            console.log(action.payload);
            let tempList = state.petList;
            for (let i in tempList) {
                if (tempList[i].Id == action.payload.Id) {
                    tempList[i].Age = action.payload.Age
                    tempList[i].Name = action.payload.Name
                    tempList[i].Description = action.payload.Description
                    break;
                }

            }
            return { ...state, petList: tempList }
        }
        case DELETE_PET: {
            let tempList = state.petList;
            console.log(action.payload);
            tempList = tempList.filter(function (item) {
                console.log(item);
                return item.Id !== action.payload.Id
            })
            console.log(tempList)
            return { ...state, petList: tempList }
        }
        default:
            return state;
    }
};

export default petData;
