import React, { useState } from 'react'
import { View, Text, SafeAreaView, Button, TextInput } from 'react-native'
import { useNavigation } from '@react-navigation/native';
import { useSelector, useDispatch } from 'react-redux'
import { ADD_PET, } from './redux/actions/index';

export default function Add(props) {
    const navigation = useNavigation();
    const dispatchRedux = useDispatch();
    const [name, setName] = useState("");
    const [age, setAge] = useState("");
    const [description, setDescription] = useState("");
    function addPetDetails() {
        if (name != "" && age != "" && description != "") {
            dispatchRedux({
                type: ADD_PET,
                payload: {
                    "Name": name,
                    "Age": age,
                    "Description": description
                }
            })
            navigation.goBack()

        }
    }
    return (
        <SafeAreaView style={{ backgroundColor: "#f2f2f2", flex: 1 }}>
            <Button title="Back" onPress={() => navigation.goBack()} />

            <Text style={{ color: 'red', fontSize: 18, margin: 20 }}> Please Enter Proper Details </Text>
            <View style={{ margin: 10, flexDirection: 'row', alignItems: 'center', }}>
                <Text style={{ fontSize: 18, flex: 0.3 }}>Name :   </Text>
                <TextInput style={{ marginLeft: 20, height: 40, width: 200, borderRadius: 5, borderWidth: 1, flex: 0.7 }}
                    onChangeText={setName}
                    value={name}
                />
            </View>
            <View style={{ margin: 10, flexDirection: 'row', alignItems: 'center', }}>
                <Text style={{ fontSize: 18, flex: 0.3 }}>Age :   </Text>
                <TextInput style={{ marginLeft: 20, height: 40, width: 200, borderRadius: 5, borderWidth: 1, flex: 0.7 }}
                    onChangeText={setAge}
                    value={age}
                />
            </View>
            <View style={{ marginBottom: 30, margin: 10, flexDirection: 'row', alignItems: 'center', }}>
                <Text style={{ fontSize: 18, flex: 0.3 }}>Description :   </Text>
                <TextInput style={{ marginLeft: 20, height: 100, width: 200, borderRadius: 5, borderWidth: 1, flex: 0.7 }}
                    onChangeText={setDescription}
                    value={description}
                    numberOfLines={10}
                    multiline={true}
                />
            </View>
            <Button title="Add" onPress={() => addPetDetails()} />
        </SafeAreaView>
    )
}
