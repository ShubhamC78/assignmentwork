import React, { useReducer, useEffect } from 'react'
import { View, Text, Button, SafeAreaView, FlatList, TouchableOpacity } from 'react-native'
import { useNavigation } from '@react-navigation/native';
import { connect } from 'react-redux';
import { useSelector, useDispatch } from 'react-redux'
import { ADD_PET, DELETE_PET } from './redux/actions/index';

export default function Home() {
    const navigation = useNavigation();
    const dispatchRedux = useDispatch();
    const petState = useSelector(state => state.petState.petList, () => { });

    function onPressAdd(name) {
        navigation.navigate("Add");
    }
    function onPressEdit(data) {
        navigation.navigate("Edit", {
            Age: data.Age,
            Description: data.Description,
            Id: data.Id,
            Name:data.Name,
        });
    }

    function onPressDog(item) {
        console.log(item);
    }


    return (
        <SafeAreaView style={{ backgroundColor: "#f2f2f2", flex: 1 }}>
            <Button title="Back" onPress={() => navigation.goBack()} />
            <View style={{ flexDirection: 'row', justifyContent: 'space-around',marginTop:20 }}>
                <CustomeButton name="Add" onPressbutton={(name) => onPressAdd(name)} />
            </View>
            <FlatList
                style={{ marginTop: 50 }}
                data={petState}
                extraData={petState}
                renderItem={({ item, index, separators }) => (
                    <View
                        key={item.Id}
                        style={{ marginHorizontal: 30, marginVertical: 10, backgroundColor: '#bdbdbd', flexDirection: 'row', borderRadius: 15, borderWidth: 1 }}
                        onPress={() => onPressDog(item)}
                    >
                        <View style={{ alignSelf: 'center', margin: 10, backgroundColor: 'white', height: 50, width: 40, borderRadius: 5, alignItems: 'center', justifyContent: 'center' }}>
                            <Text style={{ textAlign: 'center', fontSize: 12 }}>Dog  {'\n'} Image</Text>
                        </View>
                        <View style={{ margin: 10, flex: 1, }}>
                            <GetPetInfo title="Name" value={item.Name} />
                            <GetPetInfo title="Age" value={item.Age} />
                            <GetPetInfo title="Description" value={item.Description} />
                        </View>
                        <View style={{ alignItems: 'center', margin: 5 }}>
                            <TouchableOpacity style={{ backgroundColor: '#454545', alignSelf: 'center', marginRight: 10, borderColor: 'white', borderWidth: 1, borderRadius: 10 }}
                                onPress={() => dispatchRedux({
                                    type: DELETE_PET,
                                    payload: item
                                })}
                            >
                                <Text style={{ color: 'white', margin: 5, fontSize: 10 }}>
                                    Delete
                                </Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={{ margin: 5, backgroundColor: '#454545', alignSelf: 'center', marginRight: 10, borderColor: 'white', borderWidth: 1, borderRadius: 10 }}
                                onPress={() => onPressEdit(item)}
                            >
                                <Text style={{ color: 'white', margin: 5, fontSize: 10 }}>
                                    Edit
                                </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                )}
            />
        </SafeAreaView>
    )
}




export function GetPetInfo(props) {
    const { title, value } = props;
    return (
        <View style={{ flexDirection: 'row', flex: 1 }}>
            <Text style={{ flex: 0.4, fontSize: 12 }}>{title} </Text>
            <Text style={{}}>:</Text>
            <Text style={{ flex: 0.6, fontSize: 12 }} numberOfLines={4}>{value}</Text>
        </View>
    )
}


export function CustomeButton(props) {
    const { name } = props;
    return (
        <TouchableOpacity style={{ backgroundColor: '#c7c7c7', borderWidth: 1, borderRadius: 5 }} onPress={() => props.onPressbutton(name)}>
            <Text style={{ marginHorizontal: 20, marginVertical: 10 }}>
                {name}
            </Text>
        </TouchableOpacity>
    )
}